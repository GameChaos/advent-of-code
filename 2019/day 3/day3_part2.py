import numpy as np
from enum import Enum

def main():
	f = open("input.txt", "r")
	
	path1 = f.readline()
	path2 = f.readline()
	f.close()
	
	#path1 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
	#path2 = "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
	
	#path1 = "R8,U5,L5,D3"
	#path2 = "U7,R6,D4,L4"
	
	# first path
	path = path1.split(",")
	path1_points = [(0, 0)]
	position = (0, 0)
	
	for i in range(len(path)):
		move_distance = int(path[i][1:])
		move_dir = path[i][0]
		
		position = move_funcs[move_dir](move_distance, position)
		path1_points.append(position)
	
	# second path
	path = path2.split(",")
	position = (0, 0)
	path2_points = [(0, 0)]
	
	for i in range(len(path)):
		move_distance = int(path[i][1:])
		move_dir = path[i][0]
		
		position = move_funcs[move_dir](move_distance, position)
		path2_points.append(position)
	
	path1_distance = 0
	path2_distance = 0
	
	# find shortest distance
	shortest_distance = -1
	for path1_point in range(len(path1_points) - 1):
		line1 = (path1_points[path1_point], path1_points[path1_point + 1])
		line1_length = abs((line1[0][0] - line1[1][0]) + (line1[0][1] - line1[1][1]))
		
		path2_distance = 0
		
		for path2_point in range(len(path2_points) - 1):
			line2 = (path2_points[path2_point], path2_points[path2_point + 1])
			line2_length = abs((line2[0][0] - line2[1][0]) + (line2[0][1] - line2[1][1]))
			
			if lines_intersect(line1, line2):
				
				# find exact intersection point
				line1_position = (line1[0][0], line1[0][1])
				for aa in range(line1_length):
					line1_move_dir = get_move_dir(line1)
					line1_position = move_funcs[line1_move_dir](1, line1_position)
					
					line2_position = (line2[0][0], line2[0][1])
					for bb in range(line2_length):
						line2_move_dir = get_move_dir(line2)
						line2_position = move_funcs[line2_move_dir](1, line2_position)
						
						if line1_position[0] == line2_position[0] and line1_position[1] == line2_position[1]:
							distance = path1_distance + path2_distance + aa + bb + 2
							if shortest_distance == -1 or distance < shortest_distance:
								shortest_distance = distance
							break
			
			path2_distance += line2_length
		
		path1_distance += line1_length
	
	print("shortest distance is", shortest_distance)

def get_move_dir(line: tuple):
	norm_line = (line[1][0] - line[0][0], line[1][1] - line[0][1])
	if norm_line[0] > 0:
		return "R"
	elif norm_line[0] < 0:
		return "L"
	elif norm_line[1] > 0:
		return "D"
	elif norm_line[1] < 0:
		return "U"

# line = ((x1, y1), (x2, y2))
def lines_intersect(line1: tuple, line2: tuple):
	minkowski_sum = []
	
	# minkowski sum
	for i in range(len(line1)):
		for j in range(len(line2)):
			new_point = [
				line1[i][0] - line2[j][0],
				line1[i][1] - line2[j][1]
			]
			
			minkowski_sum.append(new_point)
	
	# find mins and maxs
	mins = [999999999, 999999999]
	maxs = [-999999999, -999999999]
	
	for position in minkowski_sum:
		mins = (min(mins[0], position[0]), min(mins[1], position[1]))
		maxs = (max(maxs[0], position[0]), max(maxs[1], position[1]))
	
	return (mins[0] <= 0 and maxs[0] >= 0) and (mins[1] <= 0 and maxs[1] >= 0)

def move_right(distance: int, position: tuple):
	return (position[0] + distance, position[1])

def move_left(distance: int, position: tuple):
	return (position[0] - distance, position[1])

def move_up(distance: int, position: tuple):
	return (position[0], position[1] - distance)

def move_down(distance: int, position: tuple):
	return (position[0], position[1] + distance)

move_funcs = {
	"R": move_right,
	"L": move_left,
	"U": move_up,
	"D": move_down,
}

if __name__ == "__main__":
	main()