import numpy as np
from enum import Enum

class PixelType(Enum):
	WIRE1 = 1
	WIRE2 = 2
	INTERSECTION = 3

def main():
	f = open("input.txt", "r")
	
	path1 = f.readline()
	path2 = f.readline()
	f.close()
	
	#path1 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
	#path2 = "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
	
	#path1 = "R8,U5,L5,D3"
	#path2 = "U7,R6,D4,L4"
	
	path = path1.split(",")
	position = (0, 0)
	
	# dictionary of all the coordinates where a wire is
	wire_map = {}
	
	# fill first wire's positions
	for i in range(len(path)):
		move_distance = int(path[i][1:])
		move_dir = path[i][0]
		
		for _ in range(move_distance):
			position = move_funcs[move_dir](1, position)
			wire_map[position] = PixelType.WIRE1
	
	path = path2.split(",")
	position = (0, 0)
	
	# fill second wire's positions
	shortest_distance = -1
	for i in range(len(path)):
		# distance to move
		move_distance = int(path[i][1:])
		
		# move direction (R, L, U, D)
		move_dir = path[i][0]
		
		for _ in range(move_distance):
			# move 1 pixel in one of the directions
			position = move_funcs[move_dir](1, position)
			
			# if position exists
			if position in wire_map.keys():
				# check for intersection
				if wire_map[position] != PixelType.WIRE2:
					print("lines crossed at", position, "distance is", abs(position[0]) + abs(position[1]))
					
					manhattan_distance = abs(position[0]) + abs(position[1])
					
					if shortest_distance == -1 or manhattan_distance < shortest_distance:
						shortest_distance = manhattan_distance
					
					wire_map[position] = PixelType.INTERSECTION
			else:
				wire_map[position] = PixelType.WIRE2
	
	print("shortest distance to port is", shortest_distance)
	
	# create debug image
	
	# find the most extreme coordinates of the image
	smallest_position = (999999999, 999999999)
	largest_position = (-999999999, -999999999)
	
	for position in wire_map.keys():
		smallest_position = (min(smallest_position[0], position[0]), min(smallest_position[1], position[1]))
		largest_position = (max(largest_position[0], position[0]), max(largest_position[1], position[1]))
	
	image_width = largest_position[0] - smallest_position[0] + 1
	image_height = largest_position[1] - smallest_position[1] + 1
	
	print("image width", image_width, "image height", image_height)
	
	# 1d array of pixels (blue, green, red)
	picture = np.array([1] * (image_width * image_height * 3), dtype=np.uint8)
	
	# pixel index
	pixel = 0
	
	# make the origin red
	pixel = ((0 - smallest_position[1]) * image_width + (0 - smallest_position[0])) * 3
	picture[pixel] = np.uint8(0)
	picture[pixel + 1] = np.uint8(0)
	picture[pixel + 2] = np.uint8(255)
	
	# draw all the lines to the picture array
	for pos in wire_map.keys():
		# convert 2d coordinate into 1d
		pixel = ((pos[1] - smallest_position[1]) * image_width + (pos[0] - smallest_position[0])) * 3
		colour = 255
		
		if wire_map[pos] == PixelType.WIRE1:
			colour = 127
		elif wire_map[pos] == PixelType.WIRE2:
			colour = 63
		
		picture[pixel] = np.uint8(colour) # blue
		picture[pixel + 1] = np.uint8(colour) # green
		picture[pixel + 2] = np.uint8(colour) # red
	
	# finally save the image :)
	GenerateImage("image.tga", image_width, image_height, picture)

def GenerateImage(path: str, pic_width: int, pic_height: int, image: list):
	tga_header = np.array([0, 0,
		2,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		255 & pic_width,
		255 & (pic_width >> 8),
		255 & pic_height,
		255 & (pic_height >> 8),
		24,
		32],
		dtype=np.uint8
	)
	
	f = open(path, "wb")
	f.write(tga_header)
	f.write(image)

def move_right(distance: int, position: tuple):
	return (position[0] + distance, position[1])

def move_left(distance: int, position: tuple):
	return (position[0] - distance, position[1])

def move_up(distance: int, position: tuple):
	return (position[0], position[1] - distance)

def move_down(distance: int, position: tuple):
	return (position[0], position[1] + distance)

move_funcs = {
	"R": move_right,
	"L": move_left,
	"U": move_up,
	"D": move_down,
}

if __name__ == "__main__":
	main()