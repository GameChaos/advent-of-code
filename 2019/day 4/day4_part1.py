
def main():
	start_number = 271973
	end_number = 785961
	
	password_count = 0
	
	for number in range(start_number, end_number + 1):
		digits = str(number)
		
		increasing = True
		duplicate = False
		
		for i in range(len(digits) - 1):
			digit1 = int(digits[i])
			digit2 = int(digits[i + 1])
			
			if digit1 > digit2:
				increasing = False
			if digit1 == digit2:
				duplicate = True
		
		if increasing and duplicate:
			password_count += 1
	
	print(password_count)

if __name__ == "__main__":
	main()