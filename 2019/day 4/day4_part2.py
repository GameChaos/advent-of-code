
def main():
	start_number = 271973
	end_number = 785961
	
	password_count = 0
	
	for number in range(start_number, end_number + 1):
		digits = str(number)
		
		increasing = True
		duplicate = False
		
		digit_count = len(digits)
		
		for i in range(digit_count - 1):
			digit1 = int(digits[i])
			digit2 = int(digits[i + 1])
			
			if digit1 > digit2:
				increasing = False
				break
			
			if digit1 == digit2:
				prev_different = False
				next_different = False
				
				if i == 0:
					prev_different = True
				else:
					prev_different = (int(digits[i - 1]) != digit1)
				
				if i == digit_count - 2:
					next_different = True
				else:
					next_different = (int(digits[i + 2]) != digit1)
				
				if prev_different and next_different:
					duplicate = True
		
		if increasing and duplicate:
			password_count += 1
			print(number)
	
	print(password_count)

if __name__ == "__main__":
	main()