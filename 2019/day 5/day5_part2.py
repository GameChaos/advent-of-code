import math

def main():
	
	f = open("input.txt", "r")
	
	program = f.read().split(",")
	#program = list(map(int, "1101,100,-1,4,0".split(",")))
	#program = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99".split(",")
	original_program = program.copy()
	
	address = 0
	while True:
		instruction = int(str(program[address])[-2:])
		parameter_modes = str(program[address])[:-2]
		
		if instruction in instructions.keys():
			address = instructions[instruction](program, address, parameter_modes)
		else:
			print("invalid instruction", instruction, "at address", address)
			print("program:", program)
			break
		
		if address == -1:
			print("program finished")
			#print("program:", program)
			break
	
	#print("original program:", original_program)

def get_param(program: list, address: int, parameter_mode: int):
	if parameter_mode == 1:
		return int(program[address])
	else:
		return int(program[int(program[address])])

def get_param_mode(parameter: int, parameter_modes: str):
	if parameter_modes:
		if len(parameter_modes) < parameter:
			return 0
		else:
			return int(parameter_modes[-parameter])
	else:
		return 0

def instr_add(program: list, address: int, parameter_modes: str):
	param1 = get_param(program, address + 1, get_param_mode(1, parameter_modes))
	param2 = get_param(program, address + 2, get_param_mode(2, parameter_modes))
	
	program[int(program[address + 3])] = param1 + param2
	
	return address + 4

def instr_multiply(program: list, address: int, parameter_modes: str):
	param1 = get_param(program, address + 1, get_param_mode(1, parameter_modes))
	param2 = get_param(program, address + 2, get_param_mode(2, parameter_modes))
	
	program[int(program[address + 3])] = param1 * param2
	
	return address + 4

def instr_input(program: list, address: int, parameter_modes: str):
	print("please input a number:")
	_input = int(input())
	
	try:
		_input = int(_input)
	except:
		print("invalid input", _input, "try again!")
		return instr_input(program, address, parameter_modes)
	
	program[int(program[address + 1])] = _input
	
	return address + 2

def instr_jump_if_true(program: list, address: int, parameter_modes: str):
	param1 = get_param(program, address + 1, get_param_mode(1, parameter_modes))
	param2 = get_param(program, address + 2, get_param_mode(2, parameter_modes))
	
	if param1 != 0:
		return param2
	
	return address + 3

def instr_jump_if_false(program: list, address: int, parameter_modes: str):
	param1 = get_param(program, address + 1, get_param_mode(1, parameter_modes))
	param2 = get_param(program, address + 2, get_param_mode(2, parameter_modes))
	
	if param1 == 0:
		return param2
	
	return address + 3

def instr_less_than(program: list, address: int, parameter_modes: str):
	param1 = get_param(program, address + 1, get_param_mode(1, parameter_modes))
	param2 = get_param(program, address + 2, get_param_mode(2, parameter_modes))
	
	value = 0
	if param1 < param2:
		value = 1
		
	program[int(program[address + 3])] = value
	
	return address + 4

def instr_equals(program: list, address: int, parameter_modes: str):
	param1 = get_param(program, address + 1, get_param_mode(1, parameter_modes))
	param2 = get_param(program, address + 2, get_param_mode(2, parameter_modes))
	
	value = 0
	if param1 == param2:
		value = 1
		
	program[int(program[address + 3])] = value
	
	return address + 4

def instr_output(program: list, address: int, parameter_modes: str):
	param1 = get_param(program, address + 1, get_param_mode(1, parameter_modes))
	
	print("output:", param1)
	return address + 2

def instr_finish(program: list, address: int, parameter_modes: str):
	return -1

instructions = {
	1 : instr_add,
	2 : instr_multiply,
	3 : instr_input,
	4 : instr_output,
	5 : instr_jump_if_true,
	6 : instr_jump_if_false,
	7 : instr_less_than,
	8 : instr_equals,
	99 : instr_finish
}


if __name__ == "__main__":
	main()