import math

def main():
	
	f = open("input.txt", "r")

	f1 = f.readlines()

	fuelsum = 0

	for line in f1:
		# mass of the current module
		modulemass = float(line)

		addedfuelmass = int(math.floor(modulemass / 3.0) - 2.0)
		# amout
		while addedfuelmass > 0:
			fuelsum += addedfuelmass
			addedfuelmass = int(math.floor(addedfuelmass / 3.0) - 2.0)
	
	print(fuelsum)


if __name__ == "__main__":
	main()