import math

def main():
	
	f = open("input.txt", "r")
	
	program = list(map(int, f.read().split(",")))
	original_program = program
	
	for noun in range(100):
		for verb in range(100):
			program = original_program.copy()
			program[1] = noun
			program[2] = verb

			address = 0
			while True:
				instruction = program[address]

				if instruction == 1:
					program[program[address + 3]] = program[program[address + 1]] + program[program[address + 2]]
					address += 4
					continue
				elif instruction == 2:
					program[program[address + 3]] = program[program[address + 1]] * program[program[address + 2]]
					address += 4
					continue
				elif instruction == 99:
					#print("program finished at address ", address)
					#print("program:\n", program)
					break
				else:
					print("invalid instruction ", instruction, " at address ", address)
					print("program:\n", program)
					break
			if program[0] == 19690720:
				print("Found correct verb and nouns: ", noun, " ", verb)
				print("program:\n", program)
				break
	print(original_program)


if __name__ == "__main__":
	main()